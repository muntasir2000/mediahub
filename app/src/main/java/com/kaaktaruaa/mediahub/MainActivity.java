package com.kaaktaruaa.mediahub;

import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.kaaktaruaa.mediahub.adapter.NavDrawerRecycleAdapter;
import com.kaaktaruaa.mediahub.fragment.MyFragmentPagerAdapter;

public class MainActivity extends ActionBarActivity {

    //First We Declare Titles And Icons For Our Navigation Drawer List View
    //This Icons And Titles Are holded in an Array as you can see

    String titles[] = {"Dashboard", "Movies"};
    int icons[] = {R.drawable.ic_tv, R.drawable.ic_movies};

    //Similarly we Create a String Resource for the name and email in the nav_drawer_header view
    //And we also create a int resource for profile picture in the nav_drawer_header view

    String NAME = "Meye Manush";
    String EMAIL = "amimeyemanush@human.com";
    int PROFILE = R.drawable.profile_pic;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    /* Assinging the toolbar object ot the view
    and setting the the Action bar to our toolbar
     */
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new NavDrawerRecycleAdapter(titles, icons, NAME, EMAIL, PROFILE);       // Creating the Adapter of NavDrawerRecycleAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,nav_drawer_header view name, nav_drawer_header view email,
        // and nav_drawer_header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());



                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                    Drawer.closeDrawers();
                    Toast.makeText(MainActivity.this, "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT).show();

                    return true;

                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }
        });


        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.open_drawer,R.string.close_drawer ) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }


        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

/*
        ListView listView = (ListView) findViewById(R.id.whatToWatchListView);
        HomeListViewAdapter listViewAdapter = new HomeListViewAdapter(this);

        //Dummy thumbnail
        Drawable drawableMovies = getResources().getDrawable(R.drawable.ic_movies);
        Drawable drawableTV = getResources().getDrawable(R.drawable.ic_tv);


        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(drawableMovies, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(drawableMovies, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("TV Shows");
        listViewAdapter.addItem(drawableTV, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(drawableMovies, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(drawableMovies, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("Videos");
        listViewAdapter.addItem(drawableTV, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(drawableMovies, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(drawableMovies, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("TV Shows");
        listViewAdapter.addItem(drawableTV, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(drawableMovies, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(drawableMovies, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("TV Shows");
        listViewAdapter.addItem(drawableTV, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");


        listView.setAdapter(listViewAdapter);*/
        MyFragmentPagerAdapter fragmentPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(), this);
        ViewPager viewPager = (ViewPager) findViewById(R.id.whatToWatchViewPager);
        viewPager.setAdapter(fragmentPagerAdapter);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeFragment/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }
}