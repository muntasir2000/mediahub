package com.kaaktaruaa.mediahub;

import android.graphics.drawable.Drawable;
import android.net.Uri;

/**
 * Created by tareq on 3/11/15.
 */
public class ListItem {
    public Uri thumbUri;
    public String title;
    public String description;
    public String sectionHeader;
    public String contentUrl;
}
