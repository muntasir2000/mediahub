package com.kaaktaruaa.mediahub.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by tareq on 3/10/15.
 */
public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    public MyFragmentPagerAdapter(FragmentManager fm, Context context){
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return new HomeFragment();
        }
        else if (position==1){
            return new LiveTVFragment();
        }else{
            return new MoviesFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "Home";
        }
        else if(position==1){
            return "Live TV";
        }else if( position == 2){
            return "Movies";
        }else{
            return "fuck off";
        }
    }
}
