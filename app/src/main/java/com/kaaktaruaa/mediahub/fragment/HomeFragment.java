package com.kaaktaruaa.mediahub.fragment;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kaaktaruaa.mediahub.R;
import com.kaaktaruaa.mediahub.adapter.HomeListViewAdapter;

/**
 * Created by tareq on 3/11/15.
 */
public class HomeFragment extends Fragment {
    ListView listView;
    HomeListViewAdapter listViewAdapter;

    public HomeFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.home_fragment, null);
        listView = (ListView) root.findViewById(R.id.listViewHome);
        listViewAdapter = new HomeListViewAdapter(this.getActivity());

        Uri uri = Uri.parse("http://upload.wikimedia.org/wikipedia/commons/thumb/d/de/HBO_logo.svg/2000px-HBO_logo.svg.png");

        Uri uri2 = Uri.parse("http://www.wallguds.com/wp-content/uploads/2015/02/national-geographic-wallpapers-hd-national-geographic-logo-wallpaper.jpg");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(uri, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(uri, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("TV Shows");
        listViewAdapter.addItem(uri2, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(uri, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(uri2, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("Videos");
        listViewAdapter.addItem(uri, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(uri, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(uri2, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("TV Shows");
        listViewAdapter.addItem(uri, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");

        listViewAdapter.addSectionHeader("Movies");
        listViewAdapter.addItem(uri, "Intersteller", "Another great Steven Spilberg movie");
        listViewAdapter.addItem(uri, "Contact", "Dr. Ellie Arroway, after years of searching, finds conclusive radio proof of intelligent aliens, who send plans for a mysterious machine.");

        listViewAdapter.addSectionHeader("TV Shows");
        listViewAdapter.addItem(uri2, "Big Bang Theory", "A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.");


        listView.setAdapter(listViewAdapter);

        return root;
    }

    private class Load extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listViewAdapter.clear();
        }
    }
}
