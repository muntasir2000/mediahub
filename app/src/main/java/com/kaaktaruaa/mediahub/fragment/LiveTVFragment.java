package com.kaaktaruaa.mediahub.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kaaktaruaa.mediahub.ListItem;
import com.kaaktaruaa.mediahub.R;
import com.kaaktaruaa.mediahub.adapter.LiveTVAdapter;
import com.kaaktaruaa.mediahub.datasource.AzureLoader;
import com.kaaktaruaa.mediahub.datasource.OnAzureResponseReceived;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by tareq on 3/10/15.
 */
public class LiveTVFragment extends Fragment implements OnAzureResponseReceived {
    ListView listView;
    LiveTVAdapter listViewAdapter;

    public LiveTVFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.live_tv_fragment, null);
        listView = (ListView) rootView.findViewById(R.id.live_tv_fragment_listview);
        listViewAdapter = new LiveTVAdapter(this.getActivity());

        try {
            AzureLoader.requestLoadAllChannelInfo(this, this.getActivity());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = ((ListItem) parent.getAdapter().getItem(position)).contentUrl;
             //   Toast.makeText(LiveTVFragment.this.getActivity(), url, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(url), "video/*");
                startActivity(intent);
            }
        });

        listView.setAdapter(listViewAdapter);
        return rootView;
    }

    @Override
    public void onAzureResponseReceived(List<ListItem> list) {
        listViewAdapter.addAll(list);
    }
}
