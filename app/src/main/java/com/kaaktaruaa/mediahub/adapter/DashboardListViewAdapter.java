package com.kaaktaruaa.mediahub.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaaktaruaa.mediahub.ListItem;
import com.kaaktaruaa.mediahub.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muntasir on 3/10/15.
 */
public class DashboardListViewAdapter extends BaseAdapter {
    private static final int TYPE_SECTION_HEADER = 0;
    private static final int TYPE_LIST_ITEM = 1;

    ArrayList<ListItem> data;
    ArrayList<Integer> sectionHeaderIndexes;
    LayoutInflater layoutInflater;
    Context context;

    public DashboardListViewAdapter(Context context){
        this.context = context;
        data = new ArrayList<ListItem>();
        sectionHeaderIndexes = new ArrayList<Integer>();
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(Uri bitmapUri, String title, String description){
        ListItem item = new ListItem();
        item.thumbUri = bitmapUri;
        item.title = title;
        item.description = description;

        data.add(item);
        notifyDataSetChanged();
    }

    public void addSectionHeader(String section){
        ListItem item = new ListItem();
        item.sectionHeader = section;
        data.add(item);
        sectionHeaderIndexes.add(data.size()-1);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    //todo fix viewtype count
    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeaderIndexes.contains(position)? TYPE_SECTION_HEADER : TYPE_LIST_ITEM;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = null;
        TextView tvTitle = null;
        TextView tvDescription = null;
        TextView tvSectionHeader = null;

        ListItem item = data.get(position);
        int rowType = getItemViewType(position);

        if(convertView == null){
            switch (rowType){
                case TYPE_LIST_ITEM:
                    convertView = layoutInflater.inflate(R.layout.dashboard_item_row, null);
                    imageView = (ImageView) convertView.findViewById(R.id.dashboard_item_row_thumbnail);
                    tvTitle = (TextView) convertView.findViewById(R.id.dashboard_item_row_title);
                    tvDescription = (TextView) convertView.findViewById(R.id.dashboard_item_row_description);
                    break;

                case TYPE_SECTION_HEADER:
                    convertView = layoutInflater.inflate(R.layout.dashboard_catagory_seperator, null);
                    tvSectionHeader = (TextView) convertView.findViewById(R.id.dashboard_catagory_seperator_tv);
            }
        }

        switch (rowType){
            case TYPE_LIST_ITEM:
                if(imageView!=null) {
                    Picasso.with(context).load(item.thumbUri).into(imageView);
                    tvTitle.setText(item.title);
                    tvDescription.setText(item.description);
                }
                break;
            case TYPE_SECTION_HEADER:
                if(tvSectionHeader != null) {
                    tvSectionHeader.setText(item.sectionHeader);
                }
        }
        return convertView;
    }
}

