package com.kaaktaruaa.mediahub.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaaktaruaa.mediahub.ListItem;
import com.kaaktaruaa.mediahub.R;
import com.squareup.picasso.Picasso;

import java.net.ContentHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareq on 3/11/15.
 */
public class LiveTVAdapter extends BaseAdapter {
    private ArrayList<ListItem> data;
    LayoutInflater layoutInflater;
    Context context;
    public LiveTVAdapter(Context context){
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = new ArrayList<ListItem>();
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addItem(Uri uri, String title, String description){
        ListItem item = new ListItem();
        item.thumbUri = uri;
        item.title = title;
        item.description = description;

        data.add(item);
        notifyDataSetChanged();
    }

    public void addAll(List<ListItem> list){
        data.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = null;
        TextView tvTitle = null;
        TextView tvDescription = null;
        ListItem item = data.get(position);

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.dashboard_item_row, null);
            imageView = (ImageView) convertView.findViewById(R.id.dashboard_item_row_thumbnail);
            tvTitle = (TextView) convertView.findViewById(R.id.dashboard_item_row_title);
            tvDescription = (TextView) convertView.findViewById(R.id.dashboard_item_row_description);

/*
            imageView.setImageDrawable(item.thumb);
*/
            Picasso.with(context).load(item.thumbUri).into(imageView);
            tvTitle.setText(item.title);
            tvDescription.setText(item.description);
        }


        return convertView;
    }
}
