package com.kaaktaruaa.mediahub.datasource;

/**
 * Created by tareq on 3/11/15.
 */
public interface OnResponseReceived {
    public void onResponseReceived();
}
