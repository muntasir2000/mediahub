package com.kaaktaruaa.mediahub.datasource.bean;

/**
 * Created by tareq on 3/11/15.
 */
public class Ch_Description {
    private String id;
    private int channelid;
    private String channelname;
    private String catagoryname;

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }

    public String getCatagoryname() {
        return catagoryname;
    }

    public void setCatagoryname(String catagoryname) {
        this.catagoryname = catagoryname;
    }

    public String getChannellink() {
        return channellink;
    }

    public void setChannellink(String channellink) {
        this.channellink = channellink;
    }

    private String channellink;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
