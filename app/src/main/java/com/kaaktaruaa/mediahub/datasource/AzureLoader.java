package com.kaaktaruaa.mediahub.datasource;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.kaaktaruaa.mediahub.ListItem;
import com.kaaktaruaa.mediahub.datasource.bean.Ch_Description;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by tareq on 3/11/15.
 */
public class AzureLoader {

    public static void requestLoadAllChannelInfo(final OnAzureResponseReceived parent, Context context) throws MalformedURLException, InterruptedException, ExecutionException {
        MobileServiceClient mobileServiceClient = new MobileServiceClient(AzureUtils.APP_URL, AzureUtils.APP_KEY, context);
        final MobileServiceTable<Ch_Description> channelDescriptionTable = mobileServiceClient.getTable(Ch_Description.class);
        final ArrayList<ListItem> results = new ArrayList<ListItem>();


        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                List<Ch_Description> response = null;
                try {
                    response = channelDescriptionTable.where().execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


                if(response == null){
                    return null;
                }

                for (Ch_Description channel: response){
                    ListItem item = new ListItem();
                    item.thumbUri = Uri.parse("http://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Cnn.svg/2000px-Cnn.svg.png");
                    item.title = channel.getChannelname();
                    item.description = channel.getCatagoryname();
                    item.contentUrl = channel.getChannellink();
                    results.add(item);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                parent.onAzureResponseReceived(results);
            }
        }.execute();

    }


    public static void requestLoadAllMoviesInfo(final OnAzureResponseReceived parent, Context context) throws MalformedURLException, InterruptedException, ExecutionException {
        MobileServiceClient mobileServiceClient = new MobileServiceClient(AzureUtils.APP_URL, AzureUtils.APP_KEY, context);
        final MobileServiceTable<Ch_Description> channelDescriptionTable = mobileServiceClient.getTable(Ch_Description.class);
        final ArrayList<ListItem> results = new ArrayList<ListItem>();


        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                List<Ch_Description> response = null;
                try {
                    response = channelDescriptionTable.where().execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


                if(response == null){
                    return null;
                }

                for (Ch_Description channel: response){
                    ListItem item = new ListItem();
                    item.thumbUri = Uri.parse("http://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Cnn.svg/2000px-Cnn.svg.png");
                    item.title = channel.getChannelname();
                    item.description = channel.getCatagoryname();
                    item.contentUrl = channel.getChannellink();
                    results.add(item);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                parent.onAzureResponseReceived(results);
            }
        }.execute();

    }
}
