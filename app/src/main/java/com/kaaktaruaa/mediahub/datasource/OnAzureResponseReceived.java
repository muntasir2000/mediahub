package com.kaaktaruaa.mediahub.datasource;

import com.kaaktaruaa.mediahub.ListItem;

import java.util.List;

/**
 * Created by tareq on 3/11/15.
 */
public interface OnAzureResponseReceived {
    public void onAzureResponseReceived(List<ListItem> list);
}
